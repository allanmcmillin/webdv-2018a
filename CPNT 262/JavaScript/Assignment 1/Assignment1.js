/*
Nicely done, Allan.  Your application works well and
follows almost all the business rules.  You've got
everything named correctly and I see the comments I
needed to see.  The only thing I missed was if validation
failed on entering my name or my email, the focus wasn't set
back to those inputs.
18/20
*/

function loadProvinces(){
        var provArray = ["-Select-","Alberta","British Columbia", "Manitoba","New Brunswick", "Newfoundland and Labrador","Nova Soctia","Ontario","Prince Edward Island","Quebec","Saskatchewan"];
        var selectBox;
        selectBox = document.getElementById('cboProv');
        for (index=0;index<provArray.length;index++)
        {
            var opt = document.createElement('option');
            opt.value = provArray[index];
            opt.innerHTML = provArray[index];
            selectBox.appendChild(opt);
        }
}
function validateForm(){
        // here we'll check to see if the user has filled out our elements
        //directly
        var selectBox = document.getElementById('cboProv');
        var textBox = document.getElementById('txtName');
        var emailBox = document.getElementById('txtEmail');
        if(selectBox.selectedIndex == ""){
            alert("Please select a Province");
            selectBox.focus();
            // kick user out of validation to allow user to fix error
            return;
            // Checking to see if the user entered their name
        }else if (textBox.value == "") {
            alert("Please enter your name");
            // Checking to see if the user entered their email address
        }else if (emailBox.value == "") {
            alert("Please enter your email address");
        }
    }

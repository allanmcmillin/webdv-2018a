// Well done, Allan. Your game
// looks and works great.  You've got everything
// named and working right on the backend as well.
// I see all the comments I needed to see.
// 20/20

var gamePiece =[];

function gamePieces(){
    // create and populate an array
    gamePiece=["Rock", "Paper", "Scissors", "Dynamite"];
}
var userGamePiece ="";
function userprompt(){
    // Loads the game gamePiece array
    gamePieces();
    // set the goodinput to false so that if the player don't enter an acceptable input the game will keep prompting for a choice
    goodinput=false;
    while(goodinput==false){
        userGamePiece = prompt("What do you choose Rock, Paper, Scissors, or Dynamite?","Rock");
        if (userGamePiece=="Rock"||userGamePiece=="Paper"||userGamePiece=="Scissors"||userGamePiece=="Dynamite"){
            // When the player enters an acceptable choice it will then proceed to call the whoWins function
            goodinput=true;
        }
    }
    // call the whoWins function to apply game logic to determine the winner
    whoWins();
}
// globally declare the rnd variable
var rnd;

function getRandomGamePiece(arrayLength) {
    // this is going to get a random item based off our arrays
    var rnd = Math.floor((Math.random() * arrayLength)+0);
    //return random getRandomGamePiece
    return rnd;
}

var rndGamePiece;
function whoWins(){
    // Get random  game Piece using the getRandomGamePiece function
    rndGamePiece = gamePiece[getRandomGamePiece(gamePiece.length)];
    // This is Nested Switch statement to apply game logic to choose the winner then call a
    // function to announce the winner
    switch (userGamePiece) {
        case "Rock":
            switch (rndGamePiece) {
                case "Rock":
                    itsaDraw();
                    break;
                case "Paper":
                    youLose();
                    break;
                case "Scissors":
                    youWin();
                    break;
                case "Dynamite":
                    youLose();
                    break;
                default:
            }
            break;
        case "Paper":
            switch (rndGamePiece) {
                case "Rock":
                    youWin();
                    break;
                case "Paper":
                    itsaDraw();
                    break;
                case "Scissors":
                    youLose();
                    break;
                case "Dynamite":
                    youLose();
                    break;
                default:
            }
            break;
        case "Scissors":
            switch (rndGamePiece) {
                case "Rock":
                    youLose();
                    break;
                case "Paper":
                    youWin();
                    break;
                case "Scissors":
                    itsaDraw();
                    break;
                case "Dynamite":
                    youWin();
                    break;
                default:
            }
            break;
            case "Dynamite":
                switch (rndGamePiece) {
                    case "Rock":
                        youWin();
                        break;
                    case "Paper":
                        youWin();
                        break;
                    case "Scissors":
                        youLose();
                        break;
                    case "Dynamite":
                        itsaDraw();
                        break;
                    default:
                }
                break;
        default:

    }
    // document.getElementById('results').innerHTML = rndGamePiece;

}
// If it is a draw (both oponents select the same thing)
function itsaDraw() {
// Set the Font of the results to be 30px and Different fonts based on if they win or lose then output the results
$(document).ready(function(){
    $("#results").css({
        fontFamily:"Arial",
        fontSize: "30px"
    });
    $("#results").html( userGamePiece + " vs. " + rndGamePiece + "<br>It's a Draw");
});
}
// If the player loses to the computer
function youLose() {
    $(document).ready(function(){
        $("#results").css({
            fontFamily:"Comic Sans MS",
            fontSize: "30px"
            });
        $("#results").html( userGamePiece + " vs. " + rndGamePiece + "<br>You Lose");
        });
}
// if the player is victorious over the computer
function youWin() {
// I keep this item in the comments in case I want to output the results using just javascript
//    document.getElementById('results').innerHTML = userGamePiece + " vs. " + rndGamePiece + "<br>You Win!!!"
    $(document).ready(function(){
        $("#results").css({
            fontFamily:"Impact",
            fontSize: "30px"
        });
        $("#results").html( userGamePiece + " vs. " + rndGamePiece + "<br>You Win!!!");
    });
}
